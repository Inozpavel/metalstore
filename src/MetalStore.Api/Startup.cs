using System.Data;
using MetalStore.Api.Extensions;
using FluentValidation.AspNetCore;
using Hellang.Middleware.ProblemDetails;
using MetalStore.Api.Application.Repositories;
using MetalStore.Api.Application.Repositories.Abstractions;
using MetalStore.Api.Application.UnitOfWork;
using MetalStore.Api.Utils.Filters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Npgsql;

namespace MetalStore.Api
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration) => _configuration = configuration;

        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = _configuration.GetConnectionString("DataContextConnection");
            services
                .AddMediatR()
                .AddAutoMapper()
                .AddProblemDetails();

            services.AddScoped<IDbConnection>(_ => new NpgsqlConnection(connectionString));

            services.AddScoped<IMaterialRepository, MaterialRepository>();
            services.AddScoped<IFactoryRepository, FactoryRepository>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<IRoleRepository, RoleRepository>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services
                .AddControllers(options =>
                {
                    options.Filters.Add(new ProducesAttribute("application/json"));
                    options.Filters.Add(new ExceptionFilter());
                })
                .AddJsonOptions(options => options.JsonSerializerOptions.MaxDepth = 16)
                .AddFluentValidation(options =>
                {
                    options.RegisterValidatorsFromAssemblyContaining<Startup>();
                    options.LocalizationEnabled = false;
                    options.ImplicitlyValidateChildProperties = true;
                });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseProblemDetails();

            app.UseRouting();

            app.UseEndpoints(endpoints => endpoints.MapControllers());
        }
    }
}
