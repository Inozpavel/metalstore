﻿using System;
using System.Linq;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace MetalStore.Api.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddMediatR(this IServiceCollection services)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(x => !x.IsDynamic).ToArray();
            services.AddMediatR(assemblies);

            return services;
        }

        public static IServiceCollection AddAutoMapper(this IServiceCollection services)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(x => !x.IsDynamic).ToArray();
            services.AddAutoMapper(assemblies);

            return services;
        }
    }
}
