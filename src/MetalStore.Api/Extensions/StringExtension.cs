﻿namespace MetalStore.Api.Extensions
{
    public static class StringExtension
    {
        public static string ToSqlPattern(this string @string) => $"%{@string}%";
    }
}
