﻿using System;
using System.Threading.Tasks;
using MediatR;
using MetalStore.Api.Application.Requests.Material.Create;
using MetalStore.Api.Application.Requests.Material.Get;
using MetalStore.Api.Application.Requests.Material.Update;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Swashbuckle.AspNetCore.Annotations;

namespace MetalStore.Api.Controllers
{
    [ApiController]
    [Route("api/materials")]
    [SwaggerTag("Действия с материалами")]
    public class MaterialController : ControllerBase
    {
        private readonly IMediator _mediator;

        public MaterialController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Получение списка материалов
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(CollectionModel<MaterialResponseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetMaterialsList([FromQuery] GetMaterialsListRequest request)
        {
            var result = await _mediator.Send(request);

            return Ok(result);
        }

        /// <summary>
        /// Получение материала по id
        /// </summary>
        [HttpGet("{id:guid}")]
        [ProducesResponseType(typeof(MaterialResponseModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetMaterialById(Guid id)
        {
            var request = new GetMaterialRequest(id);
            var result = await _mediator.Send(request);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        /// <summary>
        /// Создание материала
        /// </summary>
        [HttpPost]
        [ProducesResponseType(typeof(BaseCreateResponseModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateMaterial([FromBody] MaterialRequest request)
        {
            var result = await _mediator.Send(request);

            return Ok(result);
        }

        /// <summary>
        /// Изменение материала по id
        /// </summary>
        [HttpPut("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateMaterialById(Guid id, [FromBody] MaterialRequest material)
        {
            var request = new UpdateMaterialRequest
            {
                Id = id,
                Model = material,
            };

            await _mediator.Send(request);

            return Ok();
        }
    }
}
