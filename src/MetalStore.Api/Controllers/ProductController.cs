﻿using System;
using System.Threading.Tasks;
using MediatR;
using MetalStore.Api.Application.Requests.Product.Create;
using MetalStore.Api.Application.Requests.Product.Get;
using MetalStore.Api.Application.Requests.Product.Update;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace MetalStore.Api.Controllers
{
    [ApiController]
    [Route("api/products")]
    [SwaggerTag("Действия с товарами")]
    public class ProductController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ProductController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Получение списка товаров
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(CollectionModel<ProductResponseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetProductsList([FromQuery] GetProductsListRequest request)
        {
            var result = await _mediator.Send(request);

            return Ok(result);
        }

        /// <summary>
        /// Получение товара по id
        /// </summary>
        [HttpGet("{id:guid}")]
        [ProducesResponseType(typeof(ProductResponseModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetProductById(Guid id)
        {
            var request = new GetProductRequest(id);
            var result = await _mediator.Send(request);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        /// <summary>
        /// Создание товара
        /// </summary>
        [HttpPost]
        [ProducesResponseType(typeof(BaseCreateResponseModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateProduct([FromBody] ProductRequest request)
        {
            var result = await _mediator.Send(request);

            return Ok(result);
        }

        /// <summary>
        /// Изменение товара по id
        /// </summary>
        [HttpPut("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateProductById(Guid id, [FromBody] ProductRequest product)
        {
            var request = new UpdateProductRequest
            {
                Id = id,
                Model = product,
            };

            await _mediator.Send(request);

            return Ok();
        }
    }
}
