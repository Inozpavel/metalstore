﻿using System;
using System.Threading.Tasks;
using MediatR;
using MetalStore.Api.Application.Requests.Factory.Create;
using MetalStore.Api.Application.Requests.Factory.Get;
using MetalStore.Api.Application.Requests.Factory.Update;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace MetalStore.Api.Controllers
{
    [ApiController]
    [Route("api/factories")]
    [SwaggerTag("Действия с фабриками")]
    public class FactoryController : ControllerBase
    {
        private readonly IMediator _mediator;

        public FactoryController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Создание фабрики
        /// </summary>
        [HttpPost]
        [ProducesResponseType(typeof(BaseCreateResponseModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateFactory([FromBody] FactoryRequest request)
        {
            var result = await _mediator.Send(request);

            return Ok(result);
        }

        /// <summary>
        /// Получение фабрики по id
        /// </summary>
        [HttpGet("{id:guid}")]
        [ProducesResponseType(typeof(FactoryResponseModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetFactoryById(Guid id)
        {
            var request = new GetFactoryRequest(id);
            var result = await _mediator.Send(request);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        /// <summary>
        /// Получение списка фабрик
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(CollectionModel<FactoryResponseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetFactoriesList([FromQuery] GetFactoryListRequest request)
        {
            var result = await _mediator.Send(request);

            return Ok(result);
        }

        /// <summary>
        /// Изменение фабрики по id
        /// </summary>
        [HttpPut("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateMaterialById(Guid id, [FromBody] FactoryRequest material)
        {
            var request = new UpdateFactoryRequest
            {
                Id = id,
                Model = material,
            };

            await _mediator.Send(request);

            return Ok();
        }
    }
}
