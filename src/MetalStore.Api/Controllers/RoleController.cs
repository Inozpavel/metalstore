﻿using System;
using System.Threading.Tasks;
using MediatR;
using MetalStore.Api.Application.Requests.Role.Create;
using MetalStore.Api.Application.Requests.Role.Get;
using MetalStore.Api.Application.Requests.Role.Update;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace MetalStore.Api.Controllers
{
    [ApiController]
    [Route("api/roles")]
    [SwaggerTag("Действия с ролями")]
    public class RoleController : ControllerBase
    {
        private readonly IMediator _mediator;

        public RoleController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Создание роли
        /// </summary>
        [HttpPost]
        [ProducesResponseType(typeof(BaseCreateResponseModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateRole([FromBody] RoleRequest request)
        {
            var result = await _mediator.Send(request);

            return Ok(result);
        }


        /// <summary>
        /// Получение списка ролей
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(CollectionModel<RoleResponseModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetRoles([FromQuery] GetRolesListRequest request)
        {
            var result = await _mediator.Send(request);

            return Ok(result);
        }

        /// <summary>
        /// Получение роли по id
        /// </summary>
        [HttpGet("{id:guid}")]
        [ProducesResponseType(typeof(RoleResponseModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetRoleById(Guid id)
        {
            var request = new GetRoleRequest(id);
            var result = await _mediator.Send(request);

            return Ok(result);
        }

        /// <summary>
        /// Изменение роли по id
        /// </summary>
        [HttpPut("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateRoleById(Guid id, [FromBody] RoleRequest role)
        {
            var request = new UpdateRoleRequest
            {
                Id = id,
                Model = role,
            };

            await _mediator.Send(request);

            return Ok();
        }
    }
}
