﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;

namespace MetalStore.Api.Utils.Filters
{
    public class ExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            if (context.Exception is not ArgumentException argumentException)
            {
                return;
            }

            var problemDetails = new ProblemDetails
            {
                Detail = argumentException.Message,
                Status = StatusCodes.Status400BadRequest,
                Title = "Incorrect request",
            };

            var content = JsonConvert.SerializeObject(problemDetails, Formatting.Indented, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
            });

            context.Result = new ContentResult
            {
                Content = content,
                ContentType = "text/json",
                StatusCode = StatusCodes.Status400BadRequest,
            };
            context.ExceptionHandled = true;
        }
    }
}
