﻿using AutoMapper;
using MetalStore.Api.Application.Requests.Factory.Create;
using MetalStore.Api.Application.Requests.Material.Create;
using MetalStore.Api.Application.Requests.Product.Create;
using MetalStore.Api.Application.Requests.Role.Create;
using MetalStore.Api.Client.Models.EntitiesModels;
using MetalStore.Api.Entities;

namespace MetalStore.Api.Utils.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<MaterialRequest, Material>();
            CreateMap<Material, MaterialResponseModel>();

            CreateMap<ProductRequest, Product>()
                .ForMember(dest => dest.Material, opt => opt.MapFrom(src => new Material
                {
                    Id = src.MaterialId,
                }));
            CreateMap<Product, ProductResponseModel>();

            CreateMap<FactoryRequest, Factory>();
            CreateMap<Factory, FactoryResponseModel>();

            CreateMap<RoleRequest, Role>();
            CreateMap<Role, RoleResponseModel>();
        }
    }
}
