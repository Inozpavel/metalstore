﻿using System;

namespace MetalStore.Api.Entities
{
    public class Product
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public string ProductName { get; set; }

        public decimal Price { get; set; }

        public string ProductDescription { get; set; }

        public Material Material { get; set; }
    }
}
