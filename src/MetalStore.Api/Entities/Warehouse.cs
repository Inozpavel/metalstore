﻿using System;

namespace MetalStore.Api.Entities
{
    public class Warehouse
    {
        public Guid Id { get; init; } = Guid.NewGuid();

        public string WarehouseLocation { get; set; }
    }
}
