﻿using System;

namespace MetalStore.Api.Entities
{
    public class Role
    {
        public Guid Id { get; init; } = Guid.NewGuid();

        public string RoleName { get; set; }

        public string RoleDescription { get; set; }
    }
}
