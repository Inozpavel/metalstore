﻿using System;

namespace MetalStore.Api.Entities
{
    public class Factory
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public string FactoryLocation { get; set; }
    }
}
