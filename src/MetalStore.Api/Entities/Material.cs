﻿using System;

namespace MetalStore.Api.Entities
{
    public class Material
    {
        public Guid Id { get; init; } = Guid.NewGuid();

        public string MaterialName { get; set; }

        public string MaterialDescription { get; set; }

        public double LeftInStock { get; set; }
    }
}
