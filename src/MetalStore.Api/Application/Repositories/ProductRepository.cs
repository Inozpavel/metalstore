﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using MetalStore.Api.Application.Repositories.Abstractions;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Entities;
using MetalStore.Api.Extensions;

namespace MetalStore.Api.Application.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly IDbConnection _dbConnection;

        public ProductRepository(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public async Task Create(Product entity)
        {
            const string sql = @"
            INSERT INTO products (id, ProductName, ProductDescription, Price, MaterialId)
            VALUES (@id, @ProductName, @ProductDescription, @Price, @MaterialId)";

            await _dbConnection.ExecuteAsync(sql, new
            {
                entity.Id,
                entity.ProductName,
                entity.ProductDescription,
                entity.Price,
                MaterialId = entity.Material.Id,
            });
        }

        public async Task Update(Guid id, Product entity)
        {
            const string sql = @"
            UPDATE products SET
                ProductName=@ProductName,
                ProductDescription=@ProductDescription,
                MaterialId=@MaterialId
            WHERE id=@Id";


            await _dbConnection.ExecuteAsync(
                sql,
                new
                {
                    entity.Id,
                    entity.ProductName,
                    entity.ProductDescription,
                    MaterialId = entity.Material.Id,
                });
        }

        public async Task<IEnumerable<Product>> Get(PaginationWithQueryRequestModel filter)
        {
            var pattern = filter.Query.ToSqlPattern();
            const string sql = @"SELECT * FROM Products p
                                 INNER JOIN materials m on m.id = p.MaterialId
                                 WHERE p.productname ILIKE @pattern
                                 LIMIT @Limit
                                 OFFSET @Offset";

            return await _dbConnection.QueryAsync<Product, Material, Product>(
                sql,
                (product, material) =>
                {
                    product.Material = material;
                    return product;
                },
                new
                {
                    pattern,
                    filter.Limit,
                    filter.Offset,
                });
        }

        public async Task<Product> GetById(Guid id)
        {
            const string sql = @"SELECT * FROM products p
                                 INNER JOIN materials m on m.id = p.MaterialId
                                 WHERE p.id=@id";

            var entities = await _dbConnection.QueryAsync<Product, Material, Product>(sql,
                (product, material) =>
                {
                    product.Material = material;
                    return product;
                }, new
                {
                    id,
                });

            var entity = entities.FirstOrDefault();
            return entity;
        }
    }
}
