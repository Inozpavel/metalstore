﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using MetalStore.Api.Application.Repositories.Abstractions;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Entities;
using MetalStore.Api.Extensions;

namespace MetalStore.Api.Application.Repositories
{
    public class RoleRepository : IRoleRepository
    {
        private readonly IDbConnection _dbConnection;

        public RoleRepository(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public async Task Create(Role entity)
        {
            const string sql = @"
            INSERT INTO roles (id, RoleName, RoleDescription)
            VALUES (@Id, @RoleName, @RoleDescription)";
            await _dbConnection.ExecuteAsync(
                sql,
                new
                {
                    entity.Id,
                    entity.RoleName,
                    entity.RoleDescription,
                });
        }

        public async Task Update(Guid id, Role entity)
        {
            const string sql = @"
            UPDATE roles SET
                RoleName=@RoleName,
                RoleDescription=@RoleDescription
            WHERE id=@id";

            await _dbConnection.ExecuteAsync(
                sql, new
                {
                    id,
                    entity.RoleName,
                    entity.RoleDescription,
                });
        }

        public async Task<IEnumerable<Role>> Get(PaginationWithQueryRequestModel filter)
        {
            var pattern = filter.Query.ToSqlPattern();
            const string sql = @"
            SELECT * FROM roles
            WHERE RoleName ILIKE @pattern
            LIMIT @Limit
            OFFSET @Offset";

            return await _dbConnection.QueryAsync<Role>(
                sql,
                new
                {
                    pattern,
                    filter.Limit,
                    filter.Offset,
                });
        }

        public async Task<Role> GetById(Guid id)
        {
            const string sql = @"
            SELECT * FROM roles
            WHERE id=@Id
            LIMIT 1";

            var entity = await _dbConnection.QueryFirstOrDefaultAsync<Role>(sql, new
            {
                id,
            });
            return entity;
        }

        public async Task<Role> GetByName(string roleName)
        {
            const string sql = @"
            SELECT * FROM roles
            WHERE RoleName ILIKE @RoleName
            LIMIT 1";

            var entity = await _dbConnection.QueryFirstOrDefaultAsync<Role>(sql, new
            {
                roleName,
            });
            return entity;
        }
    }
}
