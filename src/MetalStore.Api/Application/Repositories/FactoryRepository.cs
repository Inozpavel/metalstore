﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using MetalStore.Api.Application.Repositories.Abstractions;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Entities;
using MetalStore.Api.Extensions;

namespace MetalStore.Api.Application.Repositories
{
    public class FactoryRepository : IFactoryRepository
    {
        private readonly IDbConnection _dbConnection;

        public FactoryRepository(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public async Task Create(Factory entity)
        {
            const string sql = @"
            INSERT INTO factories (Id, FactoryLocation) VALUES (@Id, @FactoryLocation)";
            await _dbConnection.ExecuteAsync(sql,
                new
                {
                    entity.Id,
                    entity.FactoryLocation,
                });
        }

        public async Task Update(Guid id, Factory entity)
        {
            const string sql = @"
            UPDATE factories SET FactoryLocation = @FactoryLocation
            WHERE id = @Id";
            await _dbConnection.ExecuteAsync(sql, new
            {
                entity.FactoryLocation,
            });
        }

        public async Task<IEnumerable<Factory>> Get(PaginationWithQueryRequestModel filter)
        {
            var pattern = filter.Query.ToSqlPattern();
            const string sql = @"
            SELECT * FROM factories
            WHERE FactoryLocation ILIKE @pattern
            LIMIT @Limit
            OFFSET @Offset";
            return await _dbConnection.QueryAsync<Factory>(sql, new
            {
                pattern,
                filter.Query,
                filter.Limit,
                filter.Offset,
            });
        }

        public async Task<Factory> GetById(Guid id)
        {
            const string sql = @"
            SELECT * FROM factories
            WHERE id = @Id";
            return await _dbConnection.QueryFirstOrDefaultAsync<Factory>(sql, new
            {
                id,
            });
        }

        public async Task<Factory> GetByLocation(string factoryLocation)
        {
            const string sql = @"
            SELECT * FROM factories
            WHERE FactoryLocation ILIKE @FactoryLocation";
            var result = await _dbConnection.QueryFirstOrDefaultAsync<Factory>(sql, new
            {
                factoryLocation,
            });

            return result;
        }
    }
}
