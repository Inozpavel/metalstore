﻿using System.Threading.Tasks;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Entities;

namespace MetalStore.Api.Application.Repositories.Abstractions
{
    public interface IFactoryRepository : IBaseRepository<Factory, PaginationWithQueryRequestModel>
    {
        Task<Factory> GetByLocation(string factoryLocation);
    }
}
