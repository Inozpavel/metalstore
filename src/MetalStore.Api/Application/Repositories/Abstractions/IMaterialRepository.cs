﻿using System.Threading.Tasks;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Entities;

namespace MetalStore.Api.Application.Repositories.Abstractions
{
    public interface IMaterialRepository : IBaseRepository<Material, PaginationWithQueryRequestModel>
    {
        Task<Material> FindByName(string materialName);
    }
}
