﻿using System.Threading.Tasks;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Entities;

namespace MetalStore.Api.Application.Repositories.Abstractions
{
    public interface IRoleRepository : IBaseRepository<Role, PaginationWithQueryRequestModel>
    {
        Task<Role> GetByName(string name);
    }
}
