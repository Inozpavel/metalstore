﻿using MetalStore.Api.Client.Models;
using MetalStore.Api.Entities;

namespace MetalStore.Api.Application.Repositories.Abstractions
{
    public interface IProductRepository : IBaseRepository<Product, PaginationWithQueryRequestModel>
    {
    }
}
