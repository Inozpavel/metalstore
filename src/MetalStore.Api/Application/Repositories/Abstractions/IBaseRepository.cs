﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MetalStore.Api.Application.Repositories.Abstractions
{
    public interface IBaseRepository<TEntity, in TFilter>
    {
        Task Create(TEntity entity);

        Task Update(Guid id, TEntity entity);

        Task<IEnumerable<TEntity>> Get(TFilter filter);

        Task<TEntity> GetById(Guid id);
    }
}
