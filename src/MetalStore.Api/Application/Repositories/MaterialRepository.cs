﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using MetalStore.Api.Application.Repositories.Abstractions;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Entities;
using MetalStore.Api.Extensions;

namespace MetalStore.Api.Application.Repositories
{
    public class MaterialRepository : IMaterialRepository
    {
        private readonly IDbConnection _dbConnection;

        public MaterialRepository(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public async Task Create(Material entity)
        {
            const string sql = @"
            INSERT INTO materials (id, MaterialName, MaterialDescription, LeftInStock)
            VALUES (@id, @MaterialName, @MaterialDescription, @LeftInStock)";

            await _dbConnection.ExecuteAsync(
                sql,
                new
                {
                    entity.Id,
                    entity.MaterialName,
                    entity.MaterialDescription,
                    entity.LeftInStock,
                });
        }

        public async Task Update(Guid id, Material entity)
        {
            const string sql = @"
            UPDATE Materials SET
            MaterialName=@MaterialName,
            LeftInStock=@LeftInStock,
            MaterialDescription=@MaterialDescription
            WHERE id=@id";

            await _dbConnection.ExecuteAsync(
                sql,
                new
                {
                    entity.MaterialName,
                    entity.LeftInStock,
                    entity.MaterialDescription,
                    id,
                });
        }

        public async Task<IEnumerable<Material>> Get(PaginationWithQueryRequestModel filter)
        {
            var pattern = filter.Query.ToSqlPattern();
            const string sql = @"
            SELECT * FROM materials
            WHERE MaterialName ILIKE @pattern
            LIMIT @Limit
            OFFSET @Offset";

            return await _dbConnection.QueryAsync<Material>(
                sql,
                new
                {
                    pattern,
                    filter.Limit,
                    filter.Offset,
                });
        }

        public async Task<Material> GetById(Guid id)
        {
            const string sql = "SELECT * FROM materials WHERE id=@id";
            return await _dbConnection.QueryFirstOrDefaultAsync<Material>(
                sql,
                new
                {
                    id,
                });
        }

        public async Task<Material> FindByName(string materialName)
        {
            const string sql = @"SELECT * FROM materials WHERE MaterialName ILIKE  @MaterialName";

            var existingMaterial = await _dbConnection.QueryFirstOrDefaultAsync<Material>(
                sql,
                new
                {
                    materialName,
                });

            return existingMaterial;
        }
    }
}
