﻿using MetalStore.Api.Application.Repositories.Abstractions;

namespace MetalStore.Api.Application.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(
            IMaterialRepository materialRepository,
            IFactoryRepository factoryRepository,
            IProductRepository productRepository,
            IRoleRepository roleRepository)
        {
            MaterialRepository = materialRepository;
            FactoryRepository = factoryRepository;
            ProductRepository = productRepository;
            RoleRepository = roleRepository;
        }

        public IMaterialRepository MaterialRepository { get; }

        public IFactoryRepository FactoryRepository { get; }

        public IProductRepository ProductRepository { get; }

        public IRoleRepository RoleRepository { get; }
    }
}
