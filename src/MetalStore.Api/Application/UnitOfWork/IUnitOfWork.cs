﻿using MetalStore.Api.Application.Repositories.Abstractions;

namespace MetalStore.Api.Application.UnitOfWork
{
    public interface IUnitOfWork
    {
        IMaterialRepository MaterialRepository { get; }
        IFactoryRepository FactoryRepository { get; }
        IProductRepository ProductRepository { get; }
        IRoleRepository RoleRepository  { get; }
    }
}
