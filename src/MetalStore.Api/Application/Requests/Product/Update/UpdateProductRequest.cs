﻿using MediatR;
using MetalStore.Api.Application.Requests.Product.Create;
using MetalStore.Api.Client.Models;

namespace MetalStore.Api.Application.Requests.Product.Update
{
    public class UpdateProductRequest : UpdateByIdRequestModel<ProductRequest>, IRequest
    {
    }
}
