﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using MetalStore.Api.Application.UnitOfWork;

namespace MetalStore.Api.Application.Requests.Product.Update
{
    public class UpdateProductRequestHandler : IRequestHandler<UpdateProductRequest>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateProductRequestHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(UpdateProductRequest request, CancellationToken cancellationToken)
        {
            var entity = _mapper.Map<Entities.Product>(request.Model);
            await _unitOfWork.ProductRepository.Update(request.Id, entity);

            return Unit.Value;
        }
    }
}
