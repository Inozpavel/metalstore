﻿using MediatR;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Application.Requests.Product.Create
{
    public class ProductRequest : ProductRequestModel, IRequest<BaseCreateResponseModel>
    {
    }
}
