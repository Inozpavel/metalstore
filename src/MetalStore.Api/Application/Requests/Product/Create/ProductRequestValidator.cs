﻿using FluentValidation;

namespace MetalStore.Api.Application.Requests.Product.Create
{
    public class ProductRequestValidator : AbstractValidator<ProductRequest>
    {
        public ProductRequestValidator()
        {
            RuleFor(x => x.Price)
                .GreaterThanOrEqualTo(0);

            RuleFor(x => x.ProductName)
                .NotEmpty();
        }
    }
}
