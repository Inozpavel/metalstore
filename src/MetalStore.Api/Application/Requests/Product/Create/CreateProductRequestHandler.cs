﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using MetalStore.Api.Application.UnitOfWork;
using MetalStore.Api.Client.Models;

namespace MetalStore.Api.Application.Requests.Product.Create
{
    public class CreateProductRequestHandler : IRequestHandler<ProductRequest, BaseCreateResponseModel>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public CreateProductRequestHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<BaseCreateResponseModel> Handle(ProductRequest request, CancellationToken cancellationToken)
        {
            var product = _mapper.Map<Entities.Product>(request);

            await _unitOfWork.ProductRepository.Create(product);

            return new BaseCreateResponseModel(product.Id);
        }
    }
}
