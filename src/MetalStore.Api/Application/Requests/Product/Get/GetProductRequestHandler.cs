﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using MetalStore.Api.Application.UnitOfWork;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Application.Requests.Product.Get
{
    public class GetProductRequestHandler : IRequestHandler<GetProductRequest, ProductResponseModel>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public GetProductRequestHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<ProductResponseModel> Handle(GetProductRequest request, CancellationToken cancellationToken)
        {
            var entity = await _unitOfWork.ProductRepository.GetById(request.Id);

            var responseModel = _mapper.Map<ProductResponseModel>(entity);
            return responseModel;
        }
    }
}
