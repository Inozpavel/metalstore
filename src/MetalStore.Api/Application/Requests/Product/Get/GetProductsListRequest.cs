﻿using MediatR;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Application.Requests.Product.Get
{
    public class GetProductsListRequest : PaginationWithQueryRequestModel,
        IRequest<CollectionModel<ProductResponseModel>>
    {
    }
}
