﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using MetalStore.Api.Application.UnitOfWork;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Application.Requests.Product.Get
{
    public class GetProductListRequestHandler :
        IRequestHandler<GetProductsListRequest, CollectionModel<ProductResponseModel>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public GetProductListRequestHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<CollectionModel<ProductResponseModel>> Handle(
            GetProductsListRequest request,
            CancellationToken cancellationToken)
        {
            var entities = await _unitOfWork.ProductRepository.Get(request);

            var items = _mapper.Map<ProductResponseModel[]>(entities);

            return new CollectionModel<ProductResponseModel>
            {
                Count = items.Length,
                Items = items,
            };
        }
    }
}
