﻿using FluentValidation;
using MetalStore.Api.Application.Requests.CommonValidators;

namespace MetalStore.Api.Application.Requests.Product.Get
{
    public class GetProductsListRequestValidator : AbstractValidator<GetProductsListRequest>
    {
        public GetProductsListRequestValidator()
        {
            Include(new PaginationRequestValidator());
        }
    }
}
