﻿using System;
using MediatR;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Application.Requests.Product.Get
{
    public class GetProductRequest : GetByIdRequestModel, IRequest<ProductResponseModel>
    {
        public GetProductRequest(Guid id) : base(id)
        {
        }
    }
}
