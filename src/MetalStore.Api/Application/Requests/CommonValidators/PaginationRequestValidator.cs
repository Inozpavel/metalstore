﻿using FluentValidation;
using MetalStore.Api.Client.Models;

namespace MetalStore.Api.Application.Requests.CommonValidators
{
    public class PaginationRequestValidator : AbstractValidator<PaginationRequestModel>
    {
        public PaginationRequestValidator()
        {
            RuleFor(x => x.Limit)
                .GreaterThanOrEqualTo(0);

            RuleFor(x => x.Offset)
                .GreaterThanOrEqualTo(0);
        }
    }
}
