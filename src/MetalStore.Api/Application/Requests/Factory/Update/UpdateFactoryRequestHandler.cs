﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using MetalStore.Api.Application.UnitOfWork;

namespace MetalStore.Api.Application.Requests.Factory.Update
{
    public class UpdateFactoryRequestHandler : IRequestHandler<UpdateFactoryRequest>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateFactoryRequestHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(UpdateFactoryRequest request, CancellationToken cancellationToken)
        {
            var entity = _mapper.Map<Entities.Factory>(request.Model);

            await _unitOfWork.FactoryRepository.Update(request.Id, entity);

            return Unit.Value;
        }
    }
}
