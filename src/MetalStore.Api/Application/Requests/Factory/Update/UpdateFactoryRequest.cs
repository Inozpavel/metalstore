﻿using MediatR;
using MetalStore.Api.Application.Requests.Factory.Create;
using MetalStore.Api.Client.Models;

namespace MetalStore.Api.Application.Requests.Factory.Update
{
    public class UpdateFactoryRequest : UpdateByIdRequestModel<FactoryRequest>, IRequest
    {
    }
}
