﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using MetalStore.Api.Application.UnitOfWork;
using MetalStore.Api.Client.Models;

namespace MetalStore.Api.Application.Requests.Factory.Create
{
    public class CreateFactoryRequestHandler : IRequestHandler<FactoryRequest, BaseCreateResponseModel>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public CreateFactoryRequestHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<BaseCreateResponseModel> Handle(FactoryRequest request, CancellationToken cancellationToken)
        {
            var entity = _mapper.Map<Entities.Factory>(request);

            if (await _unitOfWork.FactoryRepository.GetByLocation(entity.FactoryLocation) != null)
            {
                throw new ArgumentException($"Factory with location {entity.FactoryLocation} is already created!");
            }

            await _unitOfWork.FactoryRepository.Create(entity);

            return new BaseCreateResponseModel(entity.Id);
        }
    }
}
