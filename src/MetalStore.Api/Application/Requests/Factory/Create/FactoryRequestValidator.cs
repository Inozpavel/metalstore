﻿using FluentValidation;

namespace MetalStore.Api.Application.Requests.Factory.Create
{
    public class FactoryRequestValidator : AbstractValidator<FactoryRequest>
    {
        public FactoryRequestValidator()
        {
            RuleFor(x => x.FactoryLocation).NotEmpty();
        }
    }
}
