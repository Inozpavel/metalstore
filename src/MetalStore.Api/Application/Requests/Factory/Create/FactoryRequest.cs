﻿using MediatR;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Application.Requests.Factory.Create
{
    public class FactoryRequest : FactoryRequestModel, IRequest<BaseCreateResponseModel>
    {
    }
}
