﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using MetalStore.Api.Application.UnitOfWork;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Application.Requests.Factory.Get
{
    public class GetFactoryRequestHandler : IRequestHandler<GetFactoryRequest, FactoryResponseModel>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public GetFactoryRequestHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<FactoryResponseModel> Handle(GetFactoryRequest request, CancellationToken cancellationToken)
        {
            var entity = await _unitOfWork.FactoryRepository.GetById(request.Id);

            return _mapper.Map<FactoryResponseModel>(entity);
        }
    }
}
