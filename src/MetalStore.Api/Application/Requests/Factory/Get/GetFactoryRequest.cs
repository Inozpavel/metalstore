﻿using System;
using MediatR;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Application.Requests.Factory.Get
{
    public class GetFactoryRequest : GetByIdRequestModel, IRequest<FactoryResponseModel>
    {
        public GetFactoryRequest(Guid id) : base(id)
        {

        }
    }
}
