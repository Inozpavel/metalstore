﻿using FluentValidation;
using MetalStore.Api.Application.Requests.CommonValidators;

namespace MetalStore.Api.Application.Requests.Factory.Get
{
    public class GetFactoryListRequestValidator : AbstractValidator<GetFactoryListRequest>
    {
        public GetFactoryListRequestValidator()
        {
            Include(new PaginationRequestValidator());
        }
    }
}
