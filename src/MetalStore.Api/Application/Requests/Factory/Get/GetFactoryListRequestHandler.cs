﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using MetalStore.Api.Application.UnitOfWork;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Application.Requests.Factory.Get
{
    public class GetFactoryListRequestHandler :
        IRequestHandler<GetFactoryListRequest,
            CollectionModel<FactoryResponseModel>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public GetFactoryListRequestHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<CollectionModel<FactoryResponseModel>> Handle(
            GetFactoryListRequest request,
            CancellationToken cancellationToken)
        {
            var result = await _unitOfWork.FactoryRepository.Get(request);

            var items = _mapper.Map<FactoryResponseModel[]>(result).ToArray();
            return new CollectionModel<FactoryResponseModel>
            {
                Count = items.Length,
                Items = items,
            };
        }
    }
}
