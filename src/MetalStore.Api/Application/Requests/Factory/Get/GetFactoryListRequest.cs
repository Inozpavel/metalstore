﻿using MediatR;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Application.Requests.Factory.Get
{
    public class GetFactoryListRequest :
        PaginationWithQueryRequestModel,
        IRequest<CollectionModel<FactoryResponseModel>>
    {
    }
}
