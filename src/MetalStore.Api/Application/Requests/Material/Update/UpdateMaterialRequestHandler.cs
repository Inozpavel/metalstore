﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using MetalStore.Api.Application.UnitOfWork;

namespace MetalStore.Api.Application.Requests.Material.Update
{
    public class UpdateMaterialRequestHandler : IRequestHandler<UpdateMaterialRequest>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateMaterialRequestHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(UpdateMaterialRequest request, CancellationToken cancellationToken)
        {
            var entity = _mapper.Map<Entities.Material>(request.Model);
            await _unitOfWork.MaterialRepository.Update(request.Id, entity);
            return Unit.Value;
        }
    }
}
