﻿using MediatR;
using MetalStore.Api.Application.Requests.Material.Create;
using MetalStore.Api.Client.Models;

namespace MetalStore.Api.Application.Requests.Material.Update
{
    public class UpdateMaterialRequest : UpdateByIdRequestModel<MaterialRequest>, IRequest
    {
    }
}
