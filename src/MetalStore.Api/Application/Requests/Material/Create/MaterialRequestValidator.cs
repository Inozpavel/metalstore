﻿using FluentValidation;

namespace MetalStore.Api.Application.Requests.Material.Create
{
    public class MaterialRequestValidator : AbstractValidator<MaterialRequest>
    {
        public MaterialRequestValidator()
        {
            RuleFor(x => x.MaterialName)
                .NotEmpty();

            RuleFor(x => x.LeftInStock)
                .GreaterThanOrEqualTo(0);
        }
    }
}
