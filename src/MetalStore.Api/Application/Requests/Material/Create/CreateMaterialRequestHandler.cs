﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using MetalStore.Api.Application.UnitOfWork;
using MetalStore.Api.Client.Models;

namespace MetalStore.Api.Application.Requests.Material.Create
{
    public class CreateMaterialRequestHandler : IRequestHandler<MaterialRequest, BaseCreateResponseModel>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreateMaterialRequestHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<BaseCreateResponseModel> Handle(
            MaterialRequest request,
            CancellationToken cancellationToken)
        {
            var material = _mapper.Map<Entities.Material>(request);

            if (await _unitOfWork.MaterialRepository.FindByName(material.MaterialName) != null)
            {
                throw new ArgumentException($"Material with name {request.MaterialName} is already created!");
            }

            await _unitOfWork.MaterialRepository.Create(material);
            return new BaseCreateResponseModel(material.Id);
        }
    }
}
