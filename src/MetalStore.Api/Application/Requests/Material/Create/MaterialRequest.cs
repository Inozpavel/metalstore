﻿using MediatR;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Application.Requests.Material.Create
{
    public class MaterialRequest : MaterialRequestModel, IRequest<BaseCreateResponseModel>
    {
    }
}
