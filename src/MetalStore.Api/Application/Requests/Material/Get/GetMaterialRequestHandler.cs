﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using MetalStore.Api.Application.UnitOfWork;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Application.Requests.Material.Get
{
    public class GetMaterialRequestHandler : IRequestHandler<GetMaterialRequest, MaterialResponseModel>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public GetMaterialRequestHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<MaterialResponseModel> Handle(GetMaterialRequest request, CancellationToken cancellationToken)
        {
            var entity = await _unitOfWork.MaterialRepository.GetById(request.Id);

            var result = _mapper.Map<MaterialResponseModel>(entity);
            return result;
        }
    }
}
