﻿using FluentValidation;
using MetalStore.Api.Application.Requests.CommonValidators;

namespace MetalStore.Api.Application.Requests.Material.Get
{
    public class GetMaterialsListRequestValidator : AbstractValidator<GetMaterialsListRequest>
    {
        public GetMaterialsListRequestValidator()
        {
            Include(new PaginationRequestValidator());
        }
    }
}
