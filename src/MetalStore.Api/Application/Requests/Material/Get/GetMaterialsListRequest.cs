﻿using MediatR;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Application.Requests.Material.Get
{
    public class GetMaterialsListRequest : PaginationWithQueryRequestModel,
        IRequest<CollectionModel<MaterialResponseModel>>
    {
    }
}
