﻿using System;
using MediatR;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Application.Requests.Material.Get
{
    public class GetMaterialRequest : GetByIdRequestModel, IRequest<MaterialResponseModel>
    {
        public GetMaterialRequest(Guid id) : base(id)
        {
        }
    }
}
