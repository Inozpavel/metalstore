﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using MetalStore.Api.Application.UnitOfWork;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Application.Requests.Material.Get
{
    public class GetMaterialsListRequestHandler :
        IRequestHandler<GetMaterialsListRequest, CollectionModel<MaterialResponseModel>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetMaterialsListRequestHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<CollectionModel<MaterialResponseModel>> Handle(
            GetMaterialsListRequest request,
            CancellationToken cancellationToken)
        {
            var entities = await _unitOfWork.MaterialRepository.Get(request);
            var items = _mapper.Map<MaterialResponseModel[]>(entities).ToArray();

            return new CollectionModel<MaterialResponseModel>
            {
                Items = items,
                Count = items.Length,
            };
        }
    }
}
