﻿using MediatR;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Application.Requests.Role.Update
{
    public class UpdateRoleRequest : UpdateByIdRequestModel<RoleRequestModel>, IRequest
    {
    }
}
