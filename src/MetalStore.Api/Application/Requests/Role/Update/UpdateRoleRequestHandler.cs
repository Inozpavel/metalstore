﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using MetalStore.Api.Application.UnitOfWork;

namespace MetalStore.Api.Application.Requests.Role.Update
{
    public class UpdateRoleRequestHandler : IRequestHandler<UpdateRoleRequest>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateRoleRequestHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(UpdateRoleRequest request, CancellationToken cancellationToken)
        {
            var entity = _mapper.Map<Entities.Role>(request.Model);

            if (await _unitOfWork.RoleRepository.GetByName(entity.RoleName) != null)
            {
                throw new ArgumentException($"Role with name {entity.RoleName} is already created!");
            }

            await _unitOfWork.RoleRepository.Update(request.Id, entity);

            return Unit.Value;
        }
    }
}
