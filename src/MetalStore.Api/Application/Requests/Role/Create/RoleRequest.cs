﻿using MediatR;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Application.Requests.Role.Create
{
    public class RoleRequest : RoleRequestModel, IRequest<BaseCreateResponseModel>
    {
    }
}
