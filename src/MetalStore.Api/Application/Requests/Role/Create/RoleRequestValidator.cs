﻿using FluentValidation;

namespace MetalStore.Api.Application.Requests.Role.Create
{
    public class RoleRequestValidator : AbstractValidator<RoleRequest>
    {
        public RoleRequestValidator()
        {
            RuleFor(x => x.RoleName).NotEmpty();
        }
    }
}
