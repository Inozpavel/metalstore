﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using MetalStore.Api.Application.UnitOfWork;
using MetalStore.Api.Client.Models;

namespace MetalStore.Api.Application.Requests.Role.Create
{
    public class CreateRoleRequestHandler : IRequestHandler<RoleRequest, BaseCreateResponseModel>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public CreateRoleRequestHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<BaseCreateResponseModel> Handle(
            RoleRequest request,
            CancellationToken cancellationToken)
        {
            var entity = _mapper.Map<Entities.Role>(request);

            if (await _unitOfWork.RoleRepository.GetByName(entity.RoleName) != null)
            {
                throw new ArgumentException($"Role with name {entity.RoleName} is already created!");
            }

            await _unitOfWork.RoleRepository.Create(entity);

            return new BaseCreateResponseModel(entity.Id);
        }
    }
}
