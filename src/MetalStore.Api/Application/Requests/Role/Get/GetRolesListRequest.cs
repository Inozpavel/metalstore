﻿using MediatR;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Application.Requests.Role.Get
{
    public class GetRolesListRequest : PaginationWithQueryRequestModel, IRequest<CollectionModel<RoleResponseModel>>
    {
    }
}
