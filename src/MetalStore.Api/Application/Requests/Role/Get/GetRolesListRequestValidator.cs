﻿using FluentValidation;
using MetalStore.Api.Application.Requests.CommonValidators;

namespace MetalStore.Api.Application.Requests.Role.Get
{
    public class GetRolesListRequestValidator : AbstractValidator<GetRolesListRequest>
    {
        public GetRolesListRequestValidator()
        {
            Include(new PaginationRequestValidator());
        }
    }
}
