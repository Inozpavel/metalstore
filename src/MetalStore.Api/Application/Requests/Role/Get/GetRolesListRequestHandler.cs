﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using MetalStore.Api.Application.UnitOfWork;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Application.Requests.Role.Get
{
    public class GetRolesListRequestHandler : IRequestHandler<GetRolesListRequest, CollectionModel<RoleResponseModel>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public GetRolesListRequestHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<CollectionModel<RoleResponseModel>> Handle(
            GetRolesListRequest listRequest,
            CancellationToken cancellationToken)
        {
            var entities = await _unitOfWork.RoleRepository.Get(listRequest);
            var items = _mapper.Map<RoleResponseModel[]>(entities);

            return new CollectionModel<RoleResponseModel>
            {
                Count = items.Length,
                Items = items,
            };
        }
    }
}
