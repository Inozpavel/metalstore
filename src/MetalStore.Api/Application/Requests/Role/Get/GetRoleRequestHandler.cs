﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using MetalStore.Api.Application.UnitOfWork;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Application.Requests.Role.Get
{
    public class GetRoleRequestHandler : IRequestHandler<GetRoleRequest, RoleResponseModel>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public GetRoleRequestHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<RoleResponseModel> Handle(GetRoleRequest request, CancellationToken cancellationToken)
        {
            var entity = await _unitOfWork.RoleRepository.GetById(request.Id);

            return _mapper.Map<RoleResponseModel>(entity);
        }
    }
}
