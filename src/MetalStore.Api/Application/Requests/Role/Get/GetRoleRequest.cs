﻿using System;
using MediatR;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Application.Requests.Role.Get
{
    public class GetRoleRequest : GetByIdRequestModel, IRequest<RoleResponseModel>
    {
        public GetRoleRequest(Guid id) : base(id)
        {
        }
    }
}
