using Inozpavel.Microservices.Core;
using Microsoft.Extensions.Hosting;

namespace MetalStore.Api
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            host.Run();
        }

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseCore<Startup>();
    }
}
