﻿using System;
using System.Threading.Tasks;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;
using MetalStore.Api.Client.Models.Get;
using RestEase;

namespace MetalStore.Api.Client.Interfaces
{
    public interface IProductController
    {
        [Get("api/products")]
        Task<CollectionModel<ProductResponseModel>> GetProducts([Query] GetMaterialsListRequestModel model);

        [Get("api/products/{id}")]
        Task<ProductResponseModel> GetProductById([Path] Guid id);

        [Post("api/products")]
        Task<BaseCreateResponseModel> CreateProduct([Path] Guid id, [Body] MaterialRequestModel requestModel);

        [Put("api/products/{id}")]
        Task UpdateProductById([Path] Guid id, [Body] MaterialRequestModel requestModel);
    }
}
