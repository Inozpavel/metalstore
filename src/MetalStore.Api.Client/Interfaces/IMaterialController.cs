﻿using System;
using System.Threading.Tasks;
using MetalStore.Api.Client.Models;
using MetalStore.Api.Client.Models.EntitiesModels;
using MetalStore.Api.Client.Models.Get;
using RestEase;

namespace MetalStore.Api.Client.Interfaces
{
    public interface IMaterialController
    {
        [Get("api/materials")]
        Task<CollectionModel<MaterialResponseModel>> GetMaterials([Query] GetMaterialsListRequestModel model);

        [Get("api/materials/{id}")]
        Task<MaterialResponseModel> GetMaterialById([Path] Guid id);

        [Post("api/materials")]
        Task<BaseCreateResponseModel> CreateMaterial([Path] Guid id, [Body] MaterialRequestModel requestModel);

        [Put("api/materials/{id}")]
        Task UpdateMaterialById([Path] Guid id, [Body] MaterialRequestModel requestModel);
    }
}
