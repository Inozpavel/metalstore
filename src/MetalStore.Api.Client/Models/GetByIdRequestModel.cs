﻿using System;

namespace MetalStore.Api.Client.Models
{
    public class GetByIdRequestModel
    {
        public GetByIdRequestModel(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}
