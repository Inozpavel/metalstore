﻿using System;
using System.Collections.Generic;

namespace MetalStore.Api.Client.Models
{
    public class CollectionModel<T>
    {
        public int Count { get; set; }

        public IEnumerable<T> Items { get; set; } = Array.Empty<T>();
    }
}
