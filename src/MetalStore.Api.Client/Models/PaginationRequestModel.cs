﻿namespace MetalStore.Api.Client.Models
{
    public class PaginationRequestModel
    {
        public int Limit { get; set; } = 20;

        public int Offset { get; set; } = 0;
    }
}
