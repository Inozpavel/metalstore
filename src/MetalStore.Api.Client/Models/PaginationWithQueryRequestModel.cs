﻿namespace MetalStore.Api.Client.Models
{
    public class PaginationWithQueryRequestModel : PaginationRequestModel
    {
        public string Query { get; set; } = string.Empty;
    }
}
