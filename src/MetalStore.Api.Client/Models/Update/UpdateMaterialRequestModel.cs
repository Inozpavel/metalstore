﻿using System;
using MetalStore.Api.Client.Models.EntitiesModels;

namespace MetalStore.Api.Client.Models.Update
{
    public class UpdateMaterialRequestModel : MaterialRequestModel
    {
        public Guid Id { get; set; }

        public MaterialRequestModel Material { get; set; }
    }
}
