﻿using System;

namespace MetalStore.Api.Client.Models
{
    public class BaseCreateResponseModel
    {
        public BaseCreateResponseModel(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}
