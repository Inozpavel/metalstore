﻿using System;

namespace MetalStore.Api.Client.Models
{
    public class UpdateByIdRequestModel<T> where T : class
    {
        public Guid Id { get; set; }

        public T Model { get; set; }
    }
}
