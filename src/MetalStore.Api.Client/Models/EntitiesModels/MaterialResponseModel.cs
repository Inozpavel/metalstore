﻿using System;

namespace MetalStore.Api.Client.Models.EntitiesModels
{
    public class MaterialResponseModel
    {
        public Guid Id { get; init; }

        public string MaterialName { get; set; }

        public string MaterialDescription { get; set; }

        public double LeftInStock { get; set; }
    }
}
