﻿namespace MetalStore.Api.Client.Models.EntitiesModels
{
    public class MaterialRequestModel
    {
        public string MaterialName { get; set; }

        public double LeftInStock { get; set; }

        public string MaterialDescription { get; set; }
    }
}
