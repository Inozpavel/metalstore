﻿namespace MetalStore.Api.Client.Models.EntitiesModels
{
    public class RoleRequestModel
    {
        public string RoleName { get; set; }

        public string RoleDescription { get; set; }
    }
}
