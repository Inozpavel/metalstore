﻿using System;

namespace MetalStore.Api.Client.Models.EntitiesModels
{
    public class RoleResponseModel
    {
        public Guid Id { get; set; }

        public string RoleName { get; set; }

        public string RoleDescription { get; set; }
    }
}
