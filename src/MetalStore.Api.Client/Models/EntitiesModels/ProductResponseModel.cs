﻿using System;

namespace MetalStore.Api.Client.Models.EntitiesModels
{
    public class ProductResponseModel
    {
        public Guid Id { get; set; }

        public string ProductName { get; set; }

        public decimal Price { get; set; }

        public string ProductDescription { get; set; }

        public MaterialResponseModel Material { get; set; }
    }
}
