﻿using System;

namespace MetalStore.Api.Client.Models.EntitiesModels
{
    public class ProductRequestModel
    {
        public string ProductName { get; set; }

        public decimal Price { get; set; }

        public string ProductDescription { get; set; }

        public Guid MaterialId { get; set; }
    }
}
