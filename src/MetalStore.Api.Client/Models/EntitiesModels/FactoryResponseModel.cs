﻿using System;

namespace MetalStore.Api.Client.Models.EntitiesModels
{
    public class FactoryResponseModel
    {
        public Guid Id { get; set; }

        public string FactoryLocation { get; set; }
    }
}
