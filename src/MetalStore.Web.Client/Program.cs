using Inozpavel.Microservices.Core;
using Inozpavel.Microservices.Core.Common.Enums;
using Microsoft.Extensions.Hosting;

namespace MetalStore.Web.Client
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseCore<Startup>(environment => environment.SwaggerMode = SwaggerMode.Disabled);
    }
}
